## Changelog

### Version 1.1.4 [05/10/2017]

- Added support for shortcodes. Only works with single quotes (not double quotes) at the moment. But it has some useful applications.

### Version 1.1.3 [03/02/2017]

- Admin menu only displays for admins now (since you have to be an admin to access it anyway)

### Version 1.1.2 [03/02/2016]

- Fixed an issue with shortcodes that caused some characters, including the apostrophe, to display a preceding back slash.

### Version 1.1.1 [07/14/2015]

 - Should fix issue with tags showing up that shouldn't when they are set to false.

### Version 1.1.0 [06/22/2015]

 - Data is now only retrieved from the database once per request, and all the data is stored in wp_cache. This drastically reduces the number of database queries.
 - Cache clears when data is updated
 - Removed quark auto updater code
 - Added github auto updater code linked to bitbucket repo
 - Renamed index.php to client-info-tags.php to work with github auto updater

### Version 1.0.9 [11/28/2014]

 - Added a small check in the filter function to see if an non-string is being passed to the function.

### Version 1.0.8 [03/20/2014]

 - Fixed issue with Yoast SEO local plugins fields for single location situations.

### Version 1.0.7 [07/11/2013]

 - Fixed weird code regression

### Version 1.0.6 [07/11/2013]

 - Added the ability to use client info tags in Yoast SEO Local plugin fields.

### Version 1.0.5 [07/09/2013]

 - Fixed admin bar link on subdomain installations & multi-site installs

### Version 1.0.4 [06/05/2013]

 - Plugin now works correctly with Wordpress Multisite installs

### Version 1.0.3 [02/25/2013]

 - Fixed plugin URI in plugin notes
 - Added Client Info menu item as a top level admin page
 - Added Client Info link to admin bar that appears on front end when you're logged in
 - Values are now encoded using htmlentities() function 
 - Added edit button to top of admin page
 - Added funny little warning to edit button
 - Added warning to "done" button on edit page (warns that it won't save)
 - Added ability to wrap content in the short code and display if the value of the key isn't false

### Version 1.0.2 [02/21/2012]

 - Altered return value of cinfotg() function. It now returns false if the value of the string is "false" or empty, rather than returning an empty string
 - Made layout variable width variable
 - Made a few CSS layout tweaks
 - Fixed double loading of style.css
 - Fixed undefined variable error
 - Removed useless rearrange button, since it serves no purpose

### Version 1.0.1 [10/02/2011]

 - Added code tidbit to enable self-hosting
 - Fixed warning in edit.php

