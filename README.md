# Client Info Tags

- Contributors: Colby Sollars, Milo Jennings
- Tags: 
- Requires at least: 3.0
- Tested up to: 4.2.2
- Version: 1.1.4
- Stable tag: 1.1.4

Allows use of tags for commonly reused client info.

## Description

The client info plugin is a simple utility that allows you to replace a commonly used piece of text with a variable, allowing you to make site wide changes to these pieces of information in one central location.


## Usage

### Shortcode

```
[client-info key="key_name"]
```

For use anywhere quick tags are accepted. Will display the value corresponding to the key if it exists or a blank string if not.

### Content filter

```
{{key}}
```

Will display the value corresponding to the key if it exists or a blank string if not. For use in:

 - Post Excerpts
 - Post Titles
 - Post Content
 - Wordpress Title
 - Widget Content

### Template function

#### `cinfotag($key)`

Function will return the value

**Parameters**

```$key``` (String) Client info key you want to replace with the corresponding value.

**Example:** ```cinfotag("phone")```

**Returns:** ```555-555-5555``` or whatever that client info tag is set to. 


#### `get_cinfotag($key)`

Same functionality as above, `cinfotag($key)`, except that it echoes the value instead of returning it.