<?php
/*
 * Plugin Name: Client Info Tags
 * Plugin URI: https://bitbucket.org/nucleartuxedo/client-info-tags
 * Description: Allows use of tags for commonly reused client info
 * Version: 1.1.4
 * Author: Colby Sollars, Milo Jennings
 * Author URI: http://www.nucleartuxedo.com
 * License: GPL2
 * BitBucket Plugin URI: https://bitbucket.org/nucleartuxedo/client-info-tags
 * BitBucket Branch: master
 */

define('CLIENT_INFO_PATH', plugin_dir_path(__FILE__) );
define('CLIENT_INFO_URL', plugins_url('', __FILE__ ) );

global $client_info_version;
$client_info_version = "1.1.4";

if(!function_exists('client_info_install')){
	function client_info_install(){
		global $wpdb;
		global $client_info_version;
		$table_name = $wpdb->prefix . "client_info";
		add_option('client_info_version', $client_info_version);
		add_option('client_info_display_nontags', True);
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) return;
		$sql = "CREATE TABLE " . $table_name . " (
			id int(11) NOT NULL AUTO_INCREMENT,
			info_key varchar(255),
			info_value varchar(255),
			info_description varchar(255),
			info_category varchar(255),
			UNIQUE KEY  id (id)
			);";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
}

register_activation_hook(__FILE__,'client_info_install');

if(!function_exists('client_info_uninstall')){
	function client_info_uninstall(){
		global $wpdb;
		global $client_info_version;
		delete_option('client_info_version');
		delete_option('client_info_display_nontags');
	}
}

register_deactivation_hook(__FILE__,'client_info_uninstall');


if(!function_exists('cinfo_tags_load')) {
	function cinfo_tags_load() {
		global $wpdb;
		global $blog_id;
		$keys_set = wp_cache_get('cinfo_cache_set');
		if($keys_set === false) {
			$table_name = $wpdb->prefix . "client_info";
			$tag_info = $wpdb->get_results("SELECT info_key, info_value FROM $table_name", ARRAY_A);
			$tag_pairs = array();
			foreach($tag_info as $row)
			{
				wp_cache_set($row["info_key"], $row["info_value"], 'cinfo_tags');
			}
			wp_cache_set('cinfo_cache_set', true);
		}
	}
}

// Adding our chunks page
add_action('admin_menu', 'client_info_menu');

if(!function_exists('client_info_menu')){
	function client_info_menu() {
		$client_info_page_legacy = add_options_page('Client Info', 'Client Info', 'manage_options', 'client-info', 'client_info');
		$client_info_page = add_menu_page( "Client Info", "Client Info", "manage_options", "client-info", "client_info");
		add_action( "admin_print_scripts-$client_info_page", "client_info_admin_head" );
	}
}
add_action('admin_bar_menu', 'client_info_add_toolbar_item', 100);
function client_info_add_toolbar_item($admin_bar){
	if ( ! is_admin_bar_showing() || ! current_user_can('administrator') ) {
		return;
	}
	$admin_bar->add_menu( array(
		'id'	=> 'client-info',
		'title' => 'Client Info Tags',
		'href'	=> site_url() . '/wp-admin/admin.php?page=client-info&amp;ci_page=edit',
		'meta'	=> array(
		'title'	 => __('Client Info Tags'),
		),
	));
}

function client_info_admin_head() {
	/* Register our styles. */
	wp_register_style( 'client_info_css', CLIENT_INFO_URL.'/style.css' );
	wp_enqueue_style( 'client_info_css');
}

if(!function_exists('client_info')){
	function client_info() {
		if(!isset($_GET['ci_page'])){
			require_once(CLIENT_INFO_PATH . '/view.php');
			return;
		}
		
		if($_GET['ci_page'] == 'edit'){
			if (!current_user_can('manage_options')) {
				wp_die( __('You do not have sufficient permissions to access this page.') );
			}
	
			require_once(CLIENT_INFO_PATH . '/edit.php');
			return;
		}
	}
}

//return the tag
function cinfotag($key) {
	cinfo_tags_load();
	$value = wp_cache_get($key, 'cinfo_tags');
	if($value == false) return false;
	if( strlen($value) > 1 ){
		$value = html_entity_decode($value);
		$value = htmlentities($value);
		return $value;
	} else {
		return false;
	}
}
//echo the tag
function get_cinfotag($key){
	echo cinfotag($key);
	if($key == "false") return "";
	return;
}

function client_info_tag($attrs, $content = null )
{
	
	if(!$attrs) return $content;
	
	extract($attrs);

	cinfo_tags_load();
	
	if($key == false) return false;
	$value = wp_cache_get($key, 'cinfo_tags');
	if($value === false) return false;
	if($value === "false") return false;
	if($value){
		if($content){
			return $content;
		}
		$value = html_entity_decode($value);
		$value = htmlentities($value);
		$value = stripslashes($value);
		return $value;
	}
	return "";
}


add_shortcode('client-info', 'client_info_tag');

$client_info_filters = array(
	'get_the_excerpt',
	'single_post_title',
	'the_content',
	'the_excerpt',
	'the_title',
	'wp_title',
	'widget_content',
	'template'
);

foreach($client_info_filters as $filter)
{
	add_filter($filter, 'client_info_filter');
}

add_action('init', 'client_info_filter_local_seo_init');
function client_info_filter_local_seo_init(){
	if ( defined( 'WPSEO_VERSION' ) ) {

		add_filter( 'get_post_metadata', 'client_info_filter_local_seo_multiple_location', 10, 4 );

		function client_info_filter_local_seo_multiple_location( $nothing, $object_id, $meta_key, $single ){
			// filter by wpseo prefix
			// this filter is called a bunch of times, so it's good to kill is as quickly as possible
			if( strpos( $meta_key, '_wpseo_' ) === false)
				return;

			// Get the custom fields and store them in the WP meta cache
			$meta_cache = wp_cache_get( $object_id, 'post' . '_meta' );
			if ( !$meta_cache ){
				$meta_cache = update_meta_cache( 'post', array( $object_id ) );
				$meta_cache = $meta_cache[$object_id];
			}
			if ( !$meta_key )
				return $meta_cache;
			if ( isset( $meta_cache[$meta_key] ) ){
				if ( $single ){
					if( client_info_filter( $meta_cache[$meta_key][0] ) == 'false' )
						return false;
					else
						return client_info_filter(  $meta_cache[$meta_key][0] );
				} else {
					return array_map( 'maybe_unserialize', $meta_cache[$meta_key] );
				}
			}
			if ( $single )
				return '';
			else
				return array();
		}

		function client_info_filter_local_seo_single_location(){
			add_filter('pre_option_wpseo_local', 'client_info_filter_yoast_local');
			function client_info_filter_yoast_local($options){
				// Only filter for front end
				if( is_admin() ) return $options;

				remove_filter('pre_option_wpseo_local', 'client_info_filter_yoast_local');
				$options = get_option( 'wpseo_local' );
				foreach($options as $option => $val){
					if( client_info_filter($val) == 'false'){
						$options[$option] = false;
					} elseif( client_info_filter($val) ) {
						$options[$option] = client_info_filter($val);
					} else {
						$options[$option] = $options[$option];
					}
				}
				add_filter('pre_option_wpseo_local', 'client_info_filter_yoast_local');
				return $options;
			}
		}
		client_info_filter_local_seo_single_location();
	}
}


// Search for client info tags wrapped in double curly brackets and convert them to the value
function client_info_filter($content){

	if( is_string($content) == false ){
		return false;
	}

	cinfo_tags_load();
	
	
	$display_nontags = get_option('client_info_display_nontags');
	$pattern = "/{{(.*?)}}/";
	preg_match_all($pattern, $content, $matches);
	
	// Cancel filtering and return content if no matches found
	if(!count($matches[0]))	return $content;

	if(!count($matches[1])){
		if($display_nontags) return $content;
		foreach($matches[0] as $match){
			$content = str_replace($match, "", $content);
		}
		return do_shortcode( stripslashes( $content ) );
	}
	
	foreach($matches[1] as $match){
		$value = wp_cache_get($match, 'cinfo_tags');
		if($value === false) continue;
		$content = str_replace("{{".$match."}}", $value, $content);
	}
	if($display_nontags) return $content;
	foreach($matches[0] as $match){
		$content = str_replace($match, "", $content);
	}
	return do_shortcode( stripslashes( $content ) );
}