<?php

global $blog_id;
global $wpdb;

$table_name = $wpdb->prefix . 'client_info';
$categories  = $wpdb->get_col("SELECT DISTINCT info_category FROM $table_name");
$info_fields = array();
foreach($categories as $cat)
{
	$fields = $wpdb->get_results("SELECT * FROM $table_name WHERE info_category = '".$cat."' ORDER BY info_key", ARRAY_A);
	foreach($fields as $field)
	{
		if($field['info_key'] != "")
		{
			if(!array_key_exists($cat, $info_fields))
				$info_fields[$cat] = array();
			$info_fields[$cat][] = $field;
		}
	}
	if(array_key_exists($cat, $info_fields))
	{
		foreach($info_fields[$cat] as &$row)
		{
			foreach($row as $key => &$value)
				$value = stripslashes($value);
		}
		unset($row);
	}
}

?>

<div class="wrap">
<div class="wrap" id="custom-background">
<?php screen_icon( 'options-general' ); ?>
<h2><?php _e('Client Info'); ?><?php if (current_user_can('manage_options')) : ?><a class="button" style="margin-left:10px; top: -2px; position: relative;" href="admin.php?page=client-info&amp;ci_page=edit" >Edit<em class="hover-overlay" style="width: 240px; margin-left: -120px">with great power comes great responsibility</em></a> <?php endif ?>
</h2>
</div>
<?php if(count($info_fields)) { ?>
<div id="field-box">
	<?php foreach($info_fields as $cat => $rows) : ?>
	<div class="category-wrapper static-view">
		<div class="category-heading-container">
			<div class="category-title">
				<h3><?php echo $cat; ?></h3>
			</div>
		</div>
		<div class="field-container">
			<div class="field-headings">
				<div class="field-column1">
					Key
				</div>
				<div class="field-column2">
					Value
				</div>
				<div class="field-column3">
					Description
				</div>
			</div>
			<?php foreach($rows as $row) : ?>
			<!-- FIELD ROW -->
			<div class="field-row">
				<div class="field-inputs">
					<div class="field-column1">
						<p><input value="{{<?php echo $row['info_key']; ?>}}" readonly="readonly"></input></p>
					</div>
					<div class="field-column2">
						<p><input value="<?php echo $row['info_value']; ?>" readonly="readonly"></input></p>
					</div>
					<div class="field-column3">
						<p><input value="<?php echo $row['info_description']; ?>" readonly="readonly"></input></p>
					</div>
				</div>
			</div>
			<?php endforeach ?>
			<div style="clear:both;"></div>
		</div>
	</div>
<?php endforeach ?>
</div>
<?php } else { ?>
<i>There are no tags yet.<?php if (current_user_can('manage_options')) : ?> Click "Edit" to add some!<?php endif ?></i>
<?php } ?>
<br />
<br />
<?php if (current_user_can('manage_options')) : ?><a class="button" href="admin.php?page=client-info&amp;ci_page=edit" >Edit<em class="hover-overlay" style="width: 240px; margin-left: -120px">with great power comes great responsibility</em></a><?php endif ?>