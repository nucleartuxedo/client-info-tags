<?php

global $blog_id;
global $wpdb;
$table_name = $wpdb->prefix . "client_info";
$broken_new_sets = array();
$error_messages = array();

if(count($_POST))
{
	wp_cache_set('cinfo_cache_set', false);
	$error_messages = array();
	$wpdb->query("DELETE FROM " . $table_name);
	if(array_key_exists('new_category_name', $_POST))
	{
		if($_POST['new_category_name'] != "Create New Category" && $_POST['new_category_name'] != "")
		{
			$wpdb->insert($table_name, array(
				'info_key'=>"",
				'info_value'=>"",
				'info_description'=>"",
				'info_category'=>$_POST['new_category_name']
				));
		}
	}
	
	if(array_key_exists("category_name", $_POST))
	{
		for($i=0; $i<count($_POST['category_name']); $i++)
		{
			if($_POST['category_name'][$i])
			{
				$wpdb->insert($table_name, array(
				'info_key'=>"",
				'info_value'=>"",
				'info_description'=>"",
				'info_category'=>$_POST['category_name'][$i]
				));
			}
		}
	}
	
	if(array_key_exists("new_info_sets", $_POST))
	{
		foreach($_POST["new_info_sets"] as $new_set)
		{
			if(!$new_set["key"])
				continue;
			
			if(!$new_set["value"])
			{
				$broken_new_sets[$new_set["category"]] = $new_set;
				continue;
			}
			
			if(
				$new_set["key"] == "key field" && 
				$new_set["value"] == "value field" &&
				$new_set["description"] == "description field")
				continue;

			$wpdb->insert($table_name, array(
				'info_key'=> $new_set["key"],
				'info_value'=>$new_set["value"],
				'info_description'=>$new_set["description"],
				'info_category'=>$new_set["category"]
				));
		}
	}
	if(count($broken_new_sets))
		$error_messages[] = "At least one of your new keys has no matching value. Please review them and be sure to save.";
	
	if(array_key_exists("info_sets", $_POST))
	{
		foreach($_POST["info_sets"] as $new_set)
		{
			if(!$new_set["key"] || !$new_set["value"])
				continue;
			if(
				$new_set["key"] == "key field" && 
				$new_set["value"] == "value field" &&
				$new_set["description"] == "description field")
				continue;

			$wpdb->insert($table_name, array(
				'info_key'=> $new_set["key"],
				'info_value'=>$new_set["value"],
				'info_description'=>$new_set["description"],
				'info_category'=>$new_set["category"]
				));
		}
	}
	
	$display_nontags = isset($_POST['display_nontags']) ? True : False;
	update_option('client_info_display_nontags', $display_nontags);
}

$display_nontags = get_option('client_info_display_nontags');
$categories  = $wpdb->get_col("SELECT DISTINCT info_category FROM $table_name");
$info_fields = array();

$info_sets = 0;
$num_categories = count($categories);
foreach($categories as $cat)
{
	$info_fields[$cat] = $wpdb->get_results("SELECT * FROM $table_name WHERE info_category = '".$cat."' AND info_key != '' ORDER BY info_key", ARRAY_A);
	$info_sets+= count($info_fields[$cat]);
}
$set_count = 0;
$category_count = 0;

?>

<div class="wrap">
<div class="wrap" id="custom-background">
<?php screen_icon( 'options-general' ); ?>
<h2><?php _e('Client Info'); ?></h2>
</div>

<script type="text/javascript" >

info_set_count = <?php echo $info_sets; ?>;
category_count = <?php echo $num_categories; ?>;

function add_client_info_tag(element)
{
	new_field = "<div class='client-info-tag'> ";
	new_field+= "<input type='text' class='text-input' name='client-info-key[]' />: ";
	new_field+= "<input type='text' class='text-input' name='client-info-value[]' /> ";
	new_field+= "<input type='text' class='text-input-long' name='client-info-description[]' /> "; 
	new_field+= "<input type='button' value='remove' onclick='remove_client_tag(this)' />";
	new_field+= "</div>";
	jQuery(element).parent().find(".info_tag_list").append(new_field);
}

function remove_client_tag(element)
{
	jQuery(element).parent().remove();
}

jQuery(document).ready(function() {
	// inputs = jQuery(":input");
	// 	for(i=0; i<inputs.length; i++)
	// 	{
	// 		jQuery(inputs[i]).focus(clear_field);
	// 	}
	jQuery("#new_category_input").focus(clear_field);
	
	cat_titles = jQuery("div.category-title");
	for(i=0; i<cat_titles.length; i++) {
		jQuery(jQuery(cat_titles[i]).find("input")[0]).change(reset_categories);
	}
});

function clear_field()
{
	if(jQuery(this).attr("type") == "submit") return;
	jQuery(this).attr("value", "");
}

function add_field_row(elem_id) {
	
	category = jQuery("#new_info_set_category_"+elem_id).val();
	container = jQuery("#new_info_set_"+elem_id);
	key = jQuery(container.find("div.field-column1")[0]).find("input")[0].value;
	value = jQuery(container.find("div.field-column2")[0]).find("input")[0].value;
	desc = jQuery(container.find("div.field-column3")[0]).find("input")[0].value;
	info_set_count++;
	
	if(key == "key field" || key == "") return;
	if(value == "value field" || value == "") return;
	if(desc == "description field") desc = "";
	
	row = "";
	row+= '		<div class="field-row">'+"\n";
	row+= '			<div class="field-inputs">'+"\n";
	row+= '				<div class="field-column0 field-buttons">'+"\n";
	row+= '					<a class="remove" href=""></a>'+"\n"; 
	row+= '				</div>'+"\n";
	row+= '				<div class="field-column1">'+"\n";
	row+= '					<input type="text" maxlength="255" value="'+key+'" name="info_sets['+info_set_count+'][key]" />'+"\n";
	row+= '				</div>'+"\n";
	row+= '				<div class="field-column2">'+"\n";
	row+= '					<input type="text" maxlength="255" value="'+value+'" name="info_sets['+info_set_count+'][value]" />'+"\n";
	row+= '				</div>'+"\n";
	row+= '				<div class="field-column3">'+"\n";
	row+= '					<input type="text" maxlength="255" value="'+desc+'" name="info_sets['+info_set_count+'][description]" />'+"\n";
	row+= '				</div>'+"\n";
	row+= '				<input class="category-input" type="hidden" maxlength="255" name="info_sets['+info_set_count+'][category]" value="'+category+'" />'+"\n";
	row+= '			</div>'+"\n";
	row+= '		</div><!-- FIELD ROW CLONE -->'+"\n";
	
	jQuery(row).insertBefore(container);

	jQuery(container.find("div.field-column1")[0]).find("input")[0].value = "";
	jQuery(container.find("div.field-column2")[0]).find("input")[0].value = "";
	jQuery(container.find("div.field-column3")[0]).find("input")[0].value = "";
}

function reset_categories () {
	container = jQuery(this).parent().parent().parent();
	inputs = container.find("input.category-input");
	for(i=0; i<inputs.length; i++) {
		jQuery(inputs[i]).val(this.value);
	}
}

function add_new_category() {
	cat_name = jQuery("#new_category_input").val();
	if(cat_name == "Create New Category" || cat_name == "") return;
	jQuery("#new_category_input").val("Create New Category")
	category_count++;
	
	cat = "";
	cat+= '	<!-- EDIT EXISTING CATEGORY -->'+"\n";
	cat+= '	<div class="category-wrapper">'+"\n";
	cat+= '		<div class="category-heading-container">'+"\n";
	//cat+= '		<a href="" class="alignleft move-btn"><img src="../wp-content/plugins/client-info-tags/images/move.png"></a>'+"\n";
	cat+= '			<div class="category-title">'+"\n";
	cat+= '				<input type="text" maxlength="255" value="'+cat_name+'" name="category_name[]" />'+"\n";
	cat+= '			</div>'+"\n";
	cat+= '			<div class="category-buttons">'+"\n";
	cat+= '				<a href=""><img src="../wp-content/plugins/client-info-tags/images/remove.png"></a>'+"\n";
	cat+= '			</div>'+"\n";
	cat+= '		</div>'+"\n";
	cat+= '		<div class="field-container">'+"\n";
	cat+= '			<div class="field-headings">'+"\n";
	cat+= '				<div class="field-column1">'+"\n";
	cat+= '					Key'+"\n";
	cat+= '				</div>'+"\n";
	cat+= '				<div class="field-column2">'+"\n";
	cat+= '					Value'+"\n";
	cat+= '				</div>'+"\n";
	cat+= '				<div class="field-column3">'+"\n";
	cat+= '					Description'+"\n";
	cat+= '				</div>'+"\n";
	cat+= '			</div><!-- FIELD ROW -->'+"\n";
	cat+= '			<div id="new_info_set_'+category_count+'" class="new-field-row">'+"\n";
	cat+= '				<div class="field-inputs">'+"\n";
	cat+= '					<div class="field-column0 field-buttons">'+"\n";
	cat+= '						<a href="#" class="add" onclick="add_field_row('+category_count+')"></a>'+"\n";
	cat+= '					</div>'+"\n";
	cat+= '					<div class="field-column1">'+"\n";
	cat+= '						<input type="text" name="new_info_sets['+category_count+'][key]" maxlength="255" />'+"\n";
	cat+= '					</div>'+"\n";
	cat+= '					<div class="field-column2">'+"\n";
	cat+= '						<input type="text" name="new_info_sets['+category_count+'][value]" maxlength="255" />'+"\n";
	cat+= '					</div>'+"\n";
	cat+= '					<div class="field-column3">'+"\n";
	cat+= '						<input type="text" name="new_info_sets['+category_count+'][description]" maxlength="255" />'+"\n";
	cat+= '					</div>'+"\n";
	cat+= '					<input id="new_info_set_category_'+category_count+'" class="category-input" type="hidden" maxlength="255" name="new_info_sets['+category_count+'][category]" value="'+cat_name+'" />'+"\n";
	cat+= '			</div><!-- FIELD ROW CLONE -->'+"\n";
	cat+= '			<div style="clear:both;"></div>'+"\n";
	cat+= '		</div>'+"\n";
	cat+= '	</div>'+"\n";
	
	jQuery("div.category-wrapper:first-child").after(cat);
	//inputs = jQuery(":input");
	//for(i=0; i<inputs.length; i++)
	//{
	//	jQuery(inputs[i]).focus(clear_field);
	//}
}

function remove_field_row(elem) {
	jQuery(elem).parent().parent().remove();
}

function remove_category(elem) {
	jQuery(elem).parent().parent().parent().empty();
	jQuery(elem).parent().parent().parent().remove();
}
</script>

<?php if(count($error_messages)) { ?>

<div style="width:100%; background-color:#F99; padding:5px; margin-top:10px">
<h3 style="margin:0 0 10px 0;">Error!</h3>
<ul style="margin-left:5px">
	<?php foreach($error_messages as $error) : ?>
	<li><?php echo $error; ?></li>
	<?php endforeach ?>
</ul>
</div>

<?php } ?>

<form action="" method="post">
<div id="field-box">
	<!-- CREATE A NEW CATEGORY -->
	<div class="category-wrapper new-category category-heading-container">
		<div class="new-category-title">
			<input id="new_category_input" name="new_category_name" type="text" maxlength="255" value="Create New Category">
		</div>
		<div class="category-buttons">
			<a href="#" onclick="add_new_category()"><img src="../wp-content/plugins/client-info-tags/images/add.png"></a>
		</div>
		<div style="clear:both;"></div>
	</div>
	<?php foreach($info_fields as $cat => $rows) : ?>
	<!-- EDIT EXISTING CATEGORY -->
	<div class="category-wrapper">
		<div class="category-heading-container">
			<div class="category-title">
				<input type="text" maxlength="255" value="<?php echo $cat; ?>" name="category_name[]" />
			</div>
			<div class="category-buttons">
				<a href="#" onclick="remove_category(this)"><img src="../wp-content/plugins/client-info-tags/images/remove.png"></a>
			</div>
		</div>
		<div class="field-container">
			<div class="field-headings">
				<div class="field-column0"></div>
				<div class="field-column1">Key</div>
				<div class="field-column2">Value</div>
				<div class="field-column3">Description</div>
			</div><!-- FIELD ROW -->
			<?php foreach($rows as $row) : ?>
				<?php if($row['info_key'] == "") continue; ?>
			<div class="field-row">
				<div class="field-inputs">
					<div class="field-column0 field-buttons">
						<a href="#" class="remove" onclick="remove_field_row(this)"></a>
					</div>
					<div class="field-column1">
						<input type="text" maxlength="255" value="<?php echo $row['info_key'] ?>" name="info_sets[<?php echo $set_count; ?>][key]" />
					</div>
					<div class="field-column2">
						<input type="text" maxlength="255" value="<?php echo stripslashes($row['info_value']) ?>" name="info_sets[<?php echo $set_count; ?>][value]" />
					</div>
					<div class="field-column3">
						<input type="text" maxlength="255" value="<?php echo stripslashes($row['info_description']) ?>" name="info_sets[<?php echo $set_count; ?>][description]" />
					</div>
					<input class="category-input" type="hidden" maxlength="255" name="info_sets[<?php echo $set_count; ?>][category]" value="<?php echo $cat ?>" />
				</div>
			</div><!-- FIELD ROW CLONE -->
			<?php $set_count++; ?>
			<?php endforeach ?>
			<div id="new_info_set_<?php echo $category_count; ?>" class="new-field-row">
			<?php if(array_key_exists($cat, $broken_new_sets)) { ?>
				<div class="field-inputs">
					<div class="field-column0 field-buttons">
						<a href="#" class="add" onclick="add_field_row(<?php echo $category_count; ?>)"></a>
					</div>
					<div class="field-column1">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][key]" maxlength="255" value="<?php echo $broken_new_sets[$cat]['key']; ?>" />
					</div>
					<div class="field-column2">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][value]" maxlength="255" />
					</div>
					<div class="field-column3">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][description]" value="<?php echo $broken_new_sets[$cat]['description']; ?>" maxlength="255" />
					</div>
					<input id="new_info_set_category_<?php echo $category_count; ?>" class="category-input" type="hidden" maxlength="255" name="new_info_sets[<?php echo $category_count; ?>][category]" value="<?php echo $cat ?>" />
				</div>
			<?php } else { ?>
				<div class="field-inputs">
					<div class="field-column0 field-buttons">
						<a href="#" class="add" onclick="add_field_row(<?php echo $category_count; ?>)"></a>
					</div>
					<div class="field-column1">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][key]" maxlength="255" />
					</div>
					<div class="field-column2">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][value]" maxlength="255" />
					</div>
					<div class="field-column3">
						<input type="text" name="new_info_sets[<?php echo $category_count; ?>][description]" maxlength="255" />
					</div>
					<input id="new_info_set_category_<?php echo $category_count; ?>" class="category-input" type="hidden" maxlength="255" name="new_info_sets[<?php echo $category_count; ?>][category]" value="<?php echo $cat ?>" />
				</div>
			<?php } ?>
			</div><!-- FIELD ROW CLONE -->
			<div style="clear:both;"></div>
		</div>
	
	</div>
	<?php $category_count++; ?>
	<?php endforeach ?>
<br />
<input type="submit" class="button-primary" value="Save" />
<a class="button" style="position: relative;" href="?page=client-info" >Back<em class="hover-overlay">without saving</em></a>
</div>
</form>